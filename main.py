"""
Extraction des racines cubiques

Programme qui extrait les racines cubiques à partir du clavier.
4 cas sont traités:
1 racine réelle et deux complexes
trois racines réelles distinctes
une racine réelle et deux confondues
une racine triple.
Le programme racine_test.py tient compte de ces cas.

Source d'inspiration:
http://www.1728.org/cubic2.htm et http://www.1728.org/cubic.htm
"""

# TODO: "swifté" le programme


from utile import epsilon, trouve_racine


def main():
    while True:
        try:
            a, b, c, d, = map(float, input("Entrez les coefficients a b c d (a<>0) : ").split())
            assert (abs(a) > epsilon)

        except:
            print(F"Sortie")
            break
        print(trouve_racine(a, b, c, d))


if __name__ == '__main__':
    main()
