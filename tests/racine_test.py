import unittest
from utile import epsilon, trouve_racine


class Testfunctions(unittest.TestCase):
    def test_trois_racines_reelles(self):
        params = 2, -4, -22, 24
        racines = 4, -3, 1
        stuff_calculated = trouve_racine(*params)
        for i in range(3):
            self.assertAlmostEqual(stuff_calculated[i], racines[i], delta=epsilon)

    def test_deux_racines_complexes(self):
        params = 3, -10, 14, 27
        racines = -1, (2.1666666666666634 + 2.0749832663314605j), (2.1666666666666634 - 2.0749832663314605j)
        stuff_calculated = trouve_racine(*params)
        for i in range(3):
            self.assertAlmostEqual(stuff_calculated[i], racines[i], delta=epsilon)

    def test_une_racine_reelle_triple(self):
        params = 1, 6, 12, 8
        racines = -2, -2, -2
        stuff_calculated = trouve_racine(*params)
        for i in range(3):
            self.assertAlmostEqual(stuff_calculated[i], racines[i], delta=epsilon)

    def test_une_racine_reelle_double_sans_complexe(self):  # cas particulier de trois réelles.
        params = 2, 15, 24, -16
        racines = 0.5, -4.0, -4.0
        stuff_calculated = trouve_racine(*params)
        for i in range(3):
            self.assertAlmostEqual(stuff_calculated[i], racines[i], delta=epsilon)


if __name__ == "__main__":
    unittest.main()
