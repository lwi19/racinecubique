import math

# Module utile.py pour main.py: Extraction des racines cubiques.

epsilon = 1e-10

def autour_zero(x):
    return True if (abs(x) < epsilon) else False



def trouve_racine(a, b, c, d):
    # FIXME: Le programme plante si la valeur de H est trop près d'epsilon

    F = ((3 * c / a) - (b * b) / (a * a)) / 3
    G = (((2 * pow(b, 3)) / pow(a, 3)) - (9 * b * c) / (a * a) + (27 * d / a)) / 27
    H = (G * G) / 4 + (pow(F, 3)) / 27
    print(F,G,H)

    if H > epsilon:
        # Une racine réelle et deux racines complexes.
        print("2 complexes")
        R = -(G / 2) + math.sqrt(H)
        S = pow(R, 1 / 3)
        T = -(G / 2) - math.sqrt(H)
        U = -1 * pow(-T, 1 / 3)  # Attention: nombre complexe ici, pas de racine de nombre négatif

        x1 = (S + U) - (b / (3 * a))
        x2 = complex(-(S + U) / 2 - (b / (3 * a)), ((S - U) * math.sqrt(3)) / 2)  # =(G18-G20)*RACINE(3)/2
        x3 = complex(-(S + U) / 2 - (b / (3 * a)), -((S - U) * math.sqrt(3)) / 2)
        return x1, x2, x3

    elif autour_zero(F) and autour_zero(G) and autour_zero(H):
        # Une racine réelle triple.
        print("triple")
        x1 = x2 = x3 = -pow(d / a, 1 / 3)
        return x1, x2, x3

    else:
        # Trois racines réelles, cas dont deux peut-être confondues.
        print("3 réelles")

        I = math.sqrt((G * G) / 4 - H)
        J = pow(I, 1 / 3)
        K = math.acos(-(G / (2 * I)))
        L = -J
        M = math.cos(K / 3)
        N = math.sqrt(3) * math.sin(K / 3)
        P = -(b / (3 * a))

        x1 = 2 * J * math.cos(K / 3) - (b / (3 * a))
        x2 = L * (M + N) + P
        x3 = L * (M - N) + P
        return x1, x2, x3
